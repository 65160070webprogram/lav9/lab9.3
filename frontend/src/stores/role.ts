import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import roleService from '@/services/role'
import type { Role } from '@/types/Role'

export const useRoleStore = defineStore('role', () => {
  const loadingStore = useLoadingStore()
  const roles = ref<Role[]>([])
  const initialRole: Role = {
    name: ''
  }
  const editedrole = ref<Role>(JSON.parse(JSON.stringify(initialRole)))

  async function getRole(id: number) {
    loadingStore.doLoad()
    const res = await roleService.getRole(id)
    editedrole.value = res.data
    loadingStore.finish()
  }
  async function getRoles() {
    try {
      loadingStore.doLoad()
      const res = await roleService.getRoles()
      console.log(res.data)
      roles.value = res.data
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }
  async function saveRole() {
    loadingStore.doLoad()
    const role = editedrole.value
    if (!role.id) {
      // Add new
      console.log('Post ' + JSON.stringify(role))
      const res = await roleService.addRole(role)
    } else {
      // Update
      console.log('Patch ' + JSON.stringify(role))
      const res = await roleService.updateRole(role)
    }

    await getRoles()
    loadingStore.finish()
  }
  async function deleterole() {
    loadingStore.doLoad()
    const role = editedrole.value
    const res = await roleService.delRole(role)

    await getRoles()
    loadingStore.finish()
  }

  function clearForm() {
    editedrole.value = JSON.parse(JSON.stringify(initialRole))
  }
  return { roles, getRoles, saveRole, deleterole, editedrole, getRole, clearForm }
})
